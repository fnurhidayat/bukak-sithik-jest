const router = require('express').Router();

// Import the controller
const index = require('./controllers/indexController.js');
const post = require('./controllers/postController.js');

// Root Endpoint
router.get('/', index.index);
router.post('/posts', post.create);
router.get('/posts', post.index);

module.exports = router;
